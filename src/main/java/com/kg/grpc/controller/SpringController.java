package com.kg.grpc.controller;

import com.kg.grpc.model.NameDto;
import com.kg.grpc.model.SaluteDto;
import com.kg.grpc.service.SpringService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class SpringController {

    @Autowired
    private SpringService springService;

    @RequestMapping("/")
    public SaluteDto sayHi(@RequestBody NameDto nameDto) {
        return springService.sayHi(nameDto);
    }
}
