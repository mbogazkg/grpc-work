package com.kg.grpc;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;

import java.io.IOException;
import java.util.logging.Logger;

public class BenchmarkApp {

    static ServerApp serverApp;
    static ManagedChannel clientChannel;
    static ClientApp clientApp;
    static ApplicationContext context;

    private static final Logger logger = Logger.getLogger(BenchmarkApp.class.getName());

    public static void main(String[] args) throws IOException, InterruptedException {
        runGrpcApps();
        stopGrpcApps();

        runSpringApp(args);
        stopSpringApp();
    }

    private static void runSpringApp(String[] args) {
        long startTime = System.nanoTime();
        context = SpringApplication.run(SpringApp.class, args);
        long endTime = System.nanoTime();
        logger.info("SpringServer Execution time in milliseconds : " + (endTime - startTime) / 1000000);
    }

    private static void stopSpringApp() {
        SpringApplication.exit(context);
    }

    private static void runGrpcApps() throws IOException {
        long startTime = System.nanoTime();
        serverApp = new ServerApp();
        serverApp.start();
        long endTime = System.nanoTime();
        logger.info("GrpcServer Execution time in milliseconds : " + (endTime - startTime) / 1000000);

        String target = "localhost:50051";
        clientChannel = ManagedChannelBuilder.forTarget(target)
                .usePlaintext()
                .build();
        clientApp = new ClientApp(clientChannel);
    }

    private static void stopGrpcApps() throws InterruptedException {
        serverApp.stop();
        clientChannel.shutdown();
    }
}
