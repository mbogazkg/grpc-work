package com.kg.grpc.model;

import lombok.Data;

@Data
public class NameDto {

    public String name;

}
