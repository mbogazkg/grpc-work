package com.kg.grpc.model;

import lombok.Data;

@Data
public class SaluteDto {

    public String text;

}
