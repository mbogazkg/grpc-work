package com.kg.grpc;

import io.grpc.Channel;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;

import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ClientApp {

    private static final Logger logger = Logger.getLogger(ClientApp.class.getName());

    private final HelloWorldGrpc.HelloWorldBlockingStub blockingStub;

    static ManagedChannel channel;

    public ClientApp(Channel channel) {
        blockingStub = HelloWorldGrpc.newBlockingStub(channel);
    }

    private void greet(String name) {
        logger.info("sayHi function requesting with param : " + name + " ...");
        NameRequest request = NameRequest.newBuilder().setName(name).build();
        SaluteResponse response;
        try {
            response = blockingStub.sayHi(request);
        } catch (StatusRuntimeException e) {
            logger.log(Level.WARNING, "RPC failed: {0}", e.getStatus());
            return;
        }
        logger.info("Response: " + response.getText());
    }

    public static void main(String[] args) throws Exception {
        String user = "Mahmut";
        String target = "localhost:50051";

        // Create a communication channel to the server, known as a Channel. Channels are thread-safe
        // and reusable. It is common to create channels at the beginning of your application and reuse
        // them until the application shuts down.
        channel = ManagedChannelBuilder.forTarget(target)
                .usePlaintext()
                .build();
        try {
            ClientApp client = new ClientApp(channel);
            client.greet(user);
        } finally {
            channel.shutdownNow().awaitTermination(5, TimeUnit.SECONDS);
        }
    }
}