package com.kg.grpc;

import com.kg.grpc.service.GrpcService;
import io.grpc.Server;
import io.grpc.ServerBuilder;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ServerApp {

    private Server server;
    private int port = 50051;

    void start() throws IOException {

        Logger.getLogger("io.grpc").setLevel(Level.OFF);

        server = ServerBuilder.forPort(port)
                .addService(new GrpcService())
                .build()
                .start();

        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                // Use stderr here since the logger may have been reset by its JVM shutdown hook.
                try {
                    ServerApp.this.stop();
                } catch (InterruptedException e) {
                    e.printStackTrace(System.err);
                }
            }
        });
    }

    void stop() throws InterruptedException {
        if (server != null) {
            server.shutdown().awaitTermination(30, TimeUnit.MINUTES);
        }
    }

    void blockUntilShutdown() throws InterruptedException {
        if (server != null) {
            server.awaitTermination();
        }
    }

    public static void main(String[] args) throws IOException, InterruptedException {
        final ServerApp server = new ServerApp();
        server.start();
        server.blockUntilShutdown();
    }
}