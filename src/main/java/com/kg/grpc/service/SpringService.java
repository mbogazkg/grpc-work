package com.kg.grpc.service;

import com.kg.grpc.SaluteResponse;
import com.kg.grpc.model.NameDto;
import com.kg.grpc.model.SaluteDto;
import org.springframework.stereotype.Service;

@Service
public class SpringService {

    public SaluteDto sayHi(NameDto nameDto) {
        SaluteDto saluteResponse = new SaluteDto();
        saluteResponse.setText("Hello " + nameDto.getName() + ", Salute");

        return saluteResponse;
    }

}
