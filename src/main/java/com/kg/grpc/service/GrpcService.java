package com.kg.grpc.service;

import com.kg.grpc.HelloWorldGrpc;
import com.kg.grpc.NameRequest;
import com.kg.grpc.SaluteResponse;
import io.grpc.stub.StreamObserver;

public class GrpcService extends HelloWorldGrpc.HelloWorldImplBase {

    @Override
    public void sayHi(NameRequest req, StreamObserver<SaluteResponse> responseObserver) {
        SaluteResponse reply = SaluteResponse.newBuilder().setText("Hello " + req.getName() + ", Salute").build();
        responseObserver.onNext(reply);
        responseObserver.onCompleted();
    }

}
